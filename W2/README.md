# M120 - Mockups

# Lernziele
 - Prototyping Tools ausprobieren und kennenlernen
 - Möglichkeiten und Arten von Prototyping kennenlernen

# Sozialform
 - Zweier oder dreier Teams

# Zeit
 - 2 bis 3 Lektionen

# Produkt
 - In der Folgewoche stellen Sie Ihre Ideeen, Mockups/Wireframes und Ihren Chatbot der Klasse vor. Machen Sie sich ein paar Notizen zu Ihren Erkenntnissen. 

# Einleitung
Wie wir in der Einführungsvorlesung auf S.18 gesehen haben, gehört in die Entwicklung in der Phase "Grobkonzeption" "Visualisierung in Form von Scribbles oder interaktiven Prototypen" dazu. 
Grundsätzlich geht es darum die Anwendung aus den Anforderungen mit möglichst geringen Aufwand zu visualisieren. Das kann in Form von einfachen Skizzen erfolgen oder durch bedienbare Prototypen. Details wie das grafische Design spielen dabei anfangs keine Rolle. Das Ziel ist es in der Grobkonzeptionsphase ein Konzept zu entwickeln, dass die Anforderungen des Auftraggebers erfüllt beziehungsweise möglichst akkurat abbildet. 

Für diesen Zwischenschritt gibt es viele gute Gründe. Der Aufwand ein Wireframe oder ein Mockup zu erstellen ist viel kleiner, als die Anwendung zu programmieren. Sprich eine fundamentale Änderung im Grobkonzept kostet vielleicht ein paar Stunden arbeiten, während diesselbe Änderung in der Applikation Wochen von Arbeit vernichten kann. Weiter kann dem Kunde sehr schnell etwas präsentiert werden. Er hat früh die Möglichkeit sich die Lösung vorzustellen und wartet nicht zuerst  lange nur um festzustellen, dass seine Anforderungen nicht korrekt verstanden wurde. Ebenfalls ist es für den Programmierer viel Einfacher ein GUI mithilfe eines Wireframes zu entwickeln, anstatt aus einem textbasiereten UseCase. 
Das Schwierigste ist es häufig nicht, die Anwendungen zu programmieren, sondern die Anforderungen des Kunden zu verstehen und korrekt umzusetzen. 

Weitere Argumente und Erklärungen, weshalb es sehr sinnvoll ist Mockups / Wireframes zu arbeiten finden Sie in diesen Artikel:
 - https://www.cleveroad.com/blog/why-wireframes-are-important
 - https://www.uxpin.com/studio/blog/designers-shouldnt-neglect-mockups/
 - https://www.joomdev.com/blog/entry/benefits-of-mock-up-in-website-design


## Wireframe vs Mockup
**Wireframe:**

A wireframe is a skeletal blueprint or framework that outlines the basic design and functions of a user interface (such as a website or application).

**Mockup:**

A mockup is the next, more in-depth iteration of the wireframe outline. A mockup is a static wireframe that includes more stylistic and visual UI details to present a realistic model of what the final page or application will look like. 

Quelle: https://www.lucidchart.com/blog/wireframes-vs-mockups


# Übungen

## 1. Leseauftrag
 - 10 Minuten: Inspieren Sie sich auf https://dribbble.com/ von professionellen Designs.
 - Lesen Sie die oben verlinkten Artikel

## 2. Wireframes / Prototypes
 - Suchen Sie sich aus den Ideen für Apps etwas aus oder denken Sie sich selber eine einfache Idee in diesem Umfang aus. 
 - Suchen Sie sich ein beliebiges Wireframe oder Prototyping Tool aus. Unten finden Sie Vorschläge und Links. 
 - Versuchen Sie Ihre Idee als Wireframe zu verwirklichen
 - Beschränken Sie sich auf maximal 5 verschiedene Ansichten. 

## 3. Chatbot
In der Zukunft kommen immer weitere Formen von Benutzerschnittstellen dazu. Mit AI wird es für Computer auch immer einfacher die Menschliche Sprache "zu erlernen" ([Natural Language Processing](https://en.wikipedia.org/wiki/Natural_language_processing)). Sehr berühmt ist auch der [watson Computer](https://en.wikipedia.org/wiki/Watson_(computer)) in der [Quiz Show *Jeopardy!*](https://www.youtube.com/watch?v=Sp4q60BsHoY) gegen Menschen antritt. Die Idee dieser Übung ist, dass Sie selbst einen Prototypen für einen Chatbot erstellen. Das Ziel dabei ist es nicht einen vollständigen Prototypen zu entwickeln, sondern vielmehr eine Demo, die z.B. einem Kunden als Diskussiongrundlage gezeigt werden kann. 

 - Schauen Sie sich dieses Video an: [Support Fish](https://web.microsoftstream.com/video/b1b4ff7a-edac-425e-851e-006913390c71)
 - Ignorieren Sie den seltsamen Anwendungsfall und beurteilen Sie das Video: 
   - Was ist stimmig / unstimmig?
   - Was macht der Bot gut, was nicht?
 - Suchen Sie sich aus den Ideen für Chatbots etwas aus oder denken Sie sich selber eine einfache Idee in diesem Umfang aus. 
 - Erstellen Sie eine Simulation vom Best Case Szenario in etwa im Unfang des *Support Fish* mithilfe von [botsociety.io](https://botsociety.io/) oder einem anderen Tool. 
 - Erstellen Sie ein kurzes Video. 
   (Falls Sie keinen Export machen können oder kein Tool haben, verwenden Sie [OBS Studio](https://obsproject.com/) .)

 

## Ideen für Apps
 - Vermietung von Angelrouten
 - Backofenfernsteuerung (Temperatur, Restbackzeit, usw. )
 - Ticketreservation für eine Standseilbahn
 - WG Putzämtli App


## Ideen für Chatbots
 - Restaurantreservation
 - Impfterminvereinbarung
 - COVID-19 Selbstdiagnose

# Links

## Chatboot Prototyping Tool
 - https://botsociety.io/
 - https://chatbotsjournal.com/25-chatbot-platforms-a-comparative-table-aeefc932eaff

## Wireframe Tool
 - https://balsamiq.com/
 - https://wireframe.cc/
 - https://cliquestudios.com/free-wireframing-tools/

## Prototyping Tools
 - https://www.quant-ux.com/#/