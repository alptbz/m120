# W1 - Einführung

# Vorbereitungen
Installieren Sie die folgenden Tools:
 - [Node.js LTS Version](https://nodejs.org/en/)
 - [git](https://git-scm.com/)

# Installation angular-cli
Folgenden Befehl in der git bash ausführen:
```bash
npm install -g @angular/cli
```
Danach können Sie mit der Übung weiterfahren. 

# Übung
 - [Tour of Heroes](https://angular.io/tutorial)


# Empfohlene IDEs
 - [WebStorm](https://www.jetbrains.com/webstorm/) (Paid)
 - [Visual Studio Code](https://code.visualstudio.com/) (Free)

# Ressourcen
 - [Using Angular in Visual Studio Code](https://code.visualstudio.com/docs/nodejs/angular-tutorial)