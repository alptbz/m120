# Windows Desktop Applikation

# Codebeispiel
In der [WpfDemoApplication](https://gitlab.com/alptbz/wpfdemoapplication) werden zwei verschiedene öffentliche REST APIs eingebunden. Klonen Sie die Anwendung und sammeln Sie ein wenig Erfahrungen mit WPF und [Visual Studio Community](https://visualstudio.microsoft.com/vs/community/). 

# Liste mit REST APIs
 - [public-apis/public-apis](https://github.com/public-apis/public-apis)

# Interessante Beiträge
 - [Why you Should Choose A Web Application Over A Desktop One](https://w3-lab.com/choose-web-application-over-desktop/)
 - [Is Desktop Development Dead? Or Still Worth It?](https://insights.dice.com/2020/03/04/desktop-development-dead-still-worth-it/)
 - [Desktop vs Web](https://brandongaille.com/15-desktop-vs-web-application-pros-and-cons/)
 - [How to decide between developing a web application and a desktop](https://stackoverflow.com/questions/45292551/how-to-decide-between-developing-a-web-application-and-a-desktop-application)
 - [Why Electron is a necessary evil](https://federicoterzi.com/blog/why-electron-is-a-necessary-evil/)

# Auswahl Cross-platform Frameworks
 - [Xamarin (C#)](https://dotnet.microsoft.com/apps/xamarin)
 - [JavaFX (Java)](https://openjfx.io/)
 - [Tkinter (Python)](https://docs.python.org/3/library/tkinter.html)
 - [Electron (node.js)](https://www.electronjs.org/)
 - [Qt (C++, Python, C#, Java,...)](https://www.qt.io/)
 - [wxWidgets (C++)](https://www.wxwidgets.org/)
 - [Mono](https://www.mono-project.com/)
 
# Native Development
Auswahl von Möglichkeiten für Native App Development auf den nachfolgenden Betriebssystemen:

## Windows
 - [WPF](https://docs.microsoft.com/en-us/visualstudio/designers/getting-started-with-wpf?view=vs-2019)
 - [WinForms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netdesktop-5.0)

## macOs
 - [Cocoa](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/CocoaFundamentals/WhatIsCocoa/WhatIsCocoa.html)
 - [Quartz](https://www.xquartz.org/)
 - [Carbon](https://developer.apple.com/documentation/coreservices/carbon_core)

## Ubuntu (Linux)
- [GTK (C, JavaScript, Perl, Python, Rust, ...)](https://www.gtk.org/)

