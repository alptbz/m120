# 1. W4 - git & Markdown <!-- omit in toc -->


# 2. Inhaltsverzeichnis <!-- omit in toc -->
- [W4 - git & Markdown](#w4---git--markdown)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Vorgehen](#vorgehen)

Einführung in git:
 - https://github.com/ser-cal/M300-Git-Repo-Setup
 - https://learngitbranching.js.org/


Markdown syntax:
 - https://www.markdownguide.org/basic-syntax/


*Einfaches Markdown Beispiel:*
 - [Pinguine](Markdown%20Example.md)

# 3. Vorgehen

Grundsätzlich können Sie einen beliebigen Markdown Editor verwenden. Dieses Beispiel verwendet Visual Studio Code. 

 1. Registrieren Sie sich auf einer beliebigen Plattform (Bevorzugt: github, gitlab) und erstellen Sie dort ein neues Repository für ihre Gruppe.
 2. Klonen Sie das Repository auf Ihren lokalen Computer.
 3. Installieren Sie [Visual Studio Code](https://code.visualstudio.com/)
 4. Installieren Sie in Visual Studio Code die Plugins [Markdown All in One](https://github.com/yzhang-gh/vscode-markdown) und [Markdown Preview Enhanced](https://github.com/shd101wyy/vscode-markdown-preview-enhanced).
 5. Öffnen Sie ihr Repository als Folder in Visual Studio Code. 
 6. Mit <i>Ctrl + Shift + P</i>, geben Sie <i>Markdown: Open Preview to the Side</i> ein und Bestätigen Sie mit <i>Enter</i>
 7. Eventuell müssen Sie ihre Fenster noch richten. 

