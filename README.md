# M120 Benutzerschnittstellen implementieren

# Modulinhalt
 - [W1 - Einführung Angular](W1/README.md)
 - [W3 - Mockups und Wireframes](W2/README.md)
 - [W4 - Markdown & Git](W4/README.md)
 - [W6 - Windows Desktop Application](W6/README.md)

# Administratives
 - [Bewertungskriterien Projekt](criteria/README.md)

# Ressourcen

## User Interface Ergonomy & Design

 - Comparison of different User Interface types: [CLI / TUI / GUI / NUI](http://www.soziotech.org/vergleich-von-definitionen-fuer-natural-user-interfaces/)
 - [Ergonomy Standard ISO 9241](https://docs.google.com/presentation/d/1hNzG5-fDVzZzsikZ1HpPyG7jE1fpbyZDc68-MbyBDF4/edit?usp=sharing)
 - UX Design
   - [UX Design Steps](https://masterdesignblog.com/what-is-ux-design-explained-in-9-easy-steps/?utm_medium=social&utm_source=pinterest&utm_campaign=tailwind_tribes&utm_content=tribes&utm_term=693056156_27632402_655572)
   - [UX Principles](https://bscw.tbz.ch/bscw/bscw.cgi/d31656357/UX%20Principles.png) Psychology / Heuristic / Laws
 - UI Design
  - [UI Design-Patterns](http://ui-patterns.com/patterns)
  - [Screen Patterns](https://pttrns.com/) - Sammlung von Screenshots (Mobile)
  - 10 Rules of Good UI Design to follow ([Video](https://www.youtube.com/watch?v=RFv53AxxQAo))
  - Die 7 goldenen Regeln des UI Designs ([Artikel](https://t3n.de/news/7-goldene-regeln-ui-design-582053/))
  - [Dribbbles - For Designers](https://dribbble.com/)
 - Web Design
   - [10 Rules of Good UI Design to Follow](https://www.youtube.com/watch?v=RFv53AxxQAo)
 - Working with “[Personas](https://venngage.com/blog/user-persona-examples/)” ([Examples](https://www.justinmind.com/blog/user-persona-templates/))
 - [Serial Position Effect](https://cxl.com/blog/serial-position-effect/)

## UI Style Guide
 - [Creating a UI Style Guide](https://www.toptal.com/designers/ui/ui-styleguide-better-ux)
 - [Examples of UI Style Guide Design](https://speckyboy.com/inspirational-examples-ui-style-guides/)
 - [UI Style Guide Collection](https://bashooka.com/inspiration/40-great-examples-of-ui-style-guides/)


## Tools
 - Design Collaboration Tools [Figma](https://www.figma.com/)

# Design Patterns & Programming
 - MVC - Members ([Slides](https://docs.google.com/presentation/d/1YWjBzigXxc-49T0wRscRCSzhAMB4gWgVYFnbeDfplfk/edit?usp=sharing))
 - a lot of very good [JavaFX Resources](https://docs.google.com/document/d/11TagqcozViGI1NcUO4a2zIw_X2oWGsbA6ZtjY-YAxfo)

## User Interface Tests
 - [Introduction to GUI Testing](https://www.ranorex.com/resources/testing-wiki/gui-testing/) (good!)
 - [Introduction to GUI Testing](https://www.guru99.com/gui-testing.html) (not bad)

# GUI Testing Tools
 - Ranorex
 - Selenium ([Tutorial](https://www.guru99.com/selenium-tutorial.html))
 - QTP ([Tutorial](https://www.guru99.com/quick-test-professional-qtp-tutorial.html))
 - Cucumber ([Tutorial](https://www.guru99.com/cucumber-tutorials.html))
 - SilkTest
 - TestComplete

 - Testing as a Service: [TestingTime](https://www.testingtime.com/)
